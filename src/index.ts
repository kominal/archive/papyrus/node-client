import axios from 'axios';
import { PAPYRUS_BASE_URL, PROJECT_NAME, TENANT_ID } from './helper/environment';

export async function getTranslation(language: string, key: string, variables?: { [key: string]: any }): Promise<string> {
	if (!PAPYRUS_BASE_URL || !TENANT_ID || !PROJECT_NAME) {
		return key;
	}

	const { data } = await axios.get<{ translation: string }>(
		`${PAPYRUS_BASE_URL}/controller/tenants/${TENANT_ID}/translations/projects/${PROJECT_NAME}/languages/${language}/keys/${key}`
	);

	let { translation } = data;

	if (variables) {
		for (const key of Object.keys(variables)) {
			translation = translation.replace(new RegExp(key, 'g'), variables[key]);
		}
	}

	return translation;
}
